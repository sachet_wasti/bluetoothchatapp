package com.example.connecting_devices;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MessageAdapter extends ArrayAdapter<MessageAdapter.ViewHolder> {

    private LayoutInflater m_layoutInflator;
    private ArrayList<String> mMessage;
    private int mViewResourceId;

    static class ViewHolder{
        TextView messages;
        ViewHolder(View view){
            messages = view.findViewById(R.id.messages);
        }
    }

    public MessageAdapter(Context context, int resource, ArrayList<String> message) {
        super(context, resource);
        this.m_layoutInflator = LayoutInflater.from(context);
        this.mMessage = message;
        this.mViewResourceId = resource;
    }

    @Override
    public int getCount() {
        return mMessage.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View newView = convertView;
        MessageAdapter.ViewHolder holder = null;

        if (newView == null){
            newView = m_layoutInflator.inflate(mViewResourceId, null);
            holder = new MessageAdapter.ViewHolder(newView);
            newView.setTag(holder);
        }else {
            holder = (MessageAdapter.ViewHolder)newView.getTag();
        }

         String message = mMessage.get(position);

        holder.messages.setText(message);

        return newView;
    }

}

