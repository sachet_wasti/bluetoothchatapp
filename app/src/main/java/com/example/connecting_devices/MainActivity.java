package com.example.connecting_devices;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    /*
    The method of bluetooth communication between two devices is very simple. One should become server and open a serverSocket
    another should become client.
    HANDLERS:
        Handlers are the best way to communicate between UI main thread and background thread.
     */

    private ListView listView;
    private Button listen;
    private Button list_devices;
    private TextView status;
    private EditText writemsg;
    private Button send;
    private DeviceListAdapter deviceListAdapter;
    private MessageAdapter messageAdapter;
    private BluetoothAdapter bluetoothAdapter;

    SendReceive sendReceive;

    private ArrayList<String> messages;
    private ListView out_message;

    private static final String TAG = "MainActivity";
    Boolean bool = false;

    private interface MessageConstants {

        public static final int STATE_LISTENING = 1;
        public static final int STATE_CONNECTING = 2;
        public static final int STATE_CONNECTED = 3;
        public static final int STATE_CONNECTION_FAILED = 4;
        public static final int STATE_MESSAGE_RECEIVED = 5;
    }

    ArrayList<BluetoothDevice> arr;

    int REQUEST_ENABLE_BLUETOOTH = 1;

    private static final String APP_NAME = "BTChat";
    public static final UUID MY_UUID = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    private final BroadcastReceiver nBroadCastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent. // MAC address
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,BluetoothAdapter.ERROR);

                switch (state){

                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "onReceive: STATE_ON");
                        break;
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "onReceive: STATE_OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d(TAG, "onReceive: STATE_TURNING_ON");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d(TAG, "onReceive: STATE_TURNING_OFF");
                        break;

                }
            }
        }
    };

//    private final BroadcastReceiver receiver = new BroadcastReceiver() {
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
//                // Discovery has found a device. Get the BluetoothDevice
//                // object and its info from the Intent.
//                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//                String deviceName = device.getName();
//                String deviceHardwareAddress = device.getAddress(); // MAC address
//
//                if (!arr.contains(device))arr.add(device);
//                deviceListAdapter.notifyDataSetChanged();
//
//            }
//        }
//    };

//    private void checkBTPermission(){
//
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
//            int permission = this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
//            permission+= this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
//
//            if (permission != 0){
//                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
//            }
//        }
//
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        arr = new ArrayList<>();
        messages = new ArrayList<>();
        findViewByIds();
        enablediableBT();
        deviceListAdapter = new DeviceListAdapter(getApplicationContext(), R.layout.device_listview_adapter, arr);
        listView.setAdapter(deviceListAdapter);
        listView.setOnItemClickListener(this);
        messageAdapter = new MessageAdapter(getApplicationContext(), R.layout.message_list_adapter1, messages);
        out_message.setAdapter(messageAdapter);
        IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(nBroadCastReceiver, BTIntent);
//        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
//        registerReceiver(receiver, filter);
    }

    public void findViewByIds(){
        out_message = findViewById(R.id.out_message);
        listView = findViewById(R.id.listview);
        listen = findViewById(R.id.listen);

        listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetoothAdapter.isEnabled())enablediableBT();
                else {
                    send.setEnabled(true);
                    ServerThread serverThread = new ServerThread();
                    serverThread.start();
                }
            }
        });
        list_devices = findViewById(R.id.list_devices);
        list_devices.setOnClickListener(this);
        status = findViewById(R.id.status);
        writemsg = findViewById(R.id.writemsg);
        send = findViewById(R.id.send);
//        send.setEnabled(false);
        send.setOnClickListener(new View.OnClickListener() {
            String s;
            @Override
            public void onClick(View v) {
                if (!bluetoothAdapter.isEnabled())enablediableBT();
                else if(bluetoothAdapter.isEnabled() && bool){
                    if (!writemsg.getText().toString().equals("")){
                        s = (writemsg.getText().toString());
                    }else s = "";

                    messages.add(s);
                    messageAdapter.notifyDataSetChanged();

                    sendReceive.write(s.getBytes(Charset.defaultCharset()));
                }
            }
        });
    }

    public void enablediableBT(){

        if (bluetoothAdapter == null){
            Toast.makeText(this, "Your device does not support bluetooth", Toast.LENGTH_SHORT).show();
        }
        if (!bluetoothAdapter.isEnabled()){
            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBT);

        }
//        if (bluetoothAdapter.isEnabled()){
//
//            bluetoothAdapter.disable();
//
//            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
//            registerReceiver(nBroadCastReceiver, BTIntent);
//
//        }

    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message mssag) {

            switch (mssag.what){
                case MessageConstants.STATE_LISTENING:
                    status.setText("Listening");
                    break;
                case MessageConstants.STATE_CONNECTING:
                    status.setText("Connecting");
                    break;
                case MessageConstants.STATE_CONNECTED:
                    status.setText("Connected");
                    bool = true;
                    send.setEnabled(true);
                    break;
                case MessageConstants.STATE_CONNECTION_FAILED:
                    status.setText("Connection Failed");
                    bool = false;
                    break;
                case MessageConstants.STATE_MESSAGE_RECEIVED:
                    byte[] readbuff = (byte[])mssag.obj;
                    String tempMsg = new String(readbuff, 0, mssag.arg1);
//                    messages.clear();
                    messages.add(tempMsg);
                    messageAdapter.notifyDataSetChanged();

            }

            return true;
        }
    });

    @Override
    public void onClick(View v) {

//        if (!bluetoothAdapter.startDiscovery()) {
//            checkBTPermission();
//            bluetoothAdapter.startDiscovery();
//        }
//        if (bluetoothAdapter.isDiscovering()) {
//            Log.d(TAG, "showList: Entered isDiscovering()");
//            bluetoothAdapter.cancelDiscovery();
//
//            checkBTPermission();
//
//            bluetoothAdapter.startDiscovery();
//        }

        Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();

        for (BluetoothDevice bl : pairedDevice){
            if (!arr.contains(bl))arr.add(bl);
        }

        deviceListAdapter.notifyDataSetChanged();


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (!bluetoothAdapter.isEnabled()) enablediableBT();
        else{
            status.setText("Connecting");
            ClientThread clientThread = new ClientThread(arr.get(position));
            clientThread.start();
        }
//        send.setEnabled(true);

    }

    public class ServerThread extends Thread{
        private BluetoothServerSocket serverSocket;

        public ServerThread(){
            try {
                serverSocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord(APP_NAME, MY_UUID);
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            BluetoothSocket socket = null;

            while (true){

                try{
                    Message msg = Message.obtain();
                    msg.what = MessageConstants.STATE_CONNECTING;
                    handler.sendMessage(msg);

                    socket = serverSocket.accept();
                }catch (IOException e){

                    Message msg = Message.obtain();
                    msg.what = MessageConstants.STATE_CONNECTION_FAILED;
                    handler.sendMessage(msg);

                    e.printStackTrace();
                    break;

                }


                if (socket != null){

                    Message msg = Message.obtain();
                    msg.what = MessageConstants.STATE_CONNECTED;
                    handler.sendMessage(msg);

                    sendReceive = new SendReceive(socket);
                    sendReceive.start();

                    cancel();

                    break;


                }

            }

        }

        public void cancel() {
            try {
                serverSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(nBroadCastReceiver);
//        unregisterReceiver(receiver);
    }

    private class ClientThread extends Thread{

        private BluetoothDevice device;
        private BluetoothSocket socket;

        public ClientThread(BluetoothDevice device) {
            this.device = device;
            BluetoothSocket tmp = null;

            try{

                tmp = this.device.createRfcommSocketToServiceRecord(MY_UUID);

            }catch (IOException e){
                e.printStackTrace();
            }

            socket = tmp;
        }

        public void run(){
            try{

                socket.connect();
                Message msg = Message.obtain();
                msg.what = MessageConstants.STATE_CONNECTED;
                handler.sendMessage(msg);


            }catch (IOException e){

                try {

                    socket.close();

                }catch (IOException e1){}
                e.printStackTrace();
                Message msg = Message.obtain();
                msg.what = MessageConstants.STATE_CONNECTION_FAILED;
                handler.sendMessage(msg);
            }

            sendReceive = new SendReceive(socket);
            sendReceive.start();

        }
    }

    private class SendReceive extends Thread{

        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer;

        public SendReceive(BluetoothSocket socket){

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = mmSocket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

        }

        @Override
        public void run() {
            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // Read from the InputStream.
                    numBytes = mmInStream.read(mmBuffer);
                    // Send the obtained bytes to the UI activity.
                    Message readMsg = handler.obtainMessage(
                            MessageConstants.STATE_MESSAGE_RECEIVED, numBytes, -1,
                            mmBuffer);
                    readMsg.sendToTarget();
                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    break;
                }
            }
        }

        public void write(byte[] bytes){

            try {
                mmOutStream.write(bytes);

                int numBytes = bytes.length;

                // Share the sent message with the UI activity.
                Message writtenMsg = handler.obtainMessage(
                        MessageConstants.STATE_MESSAGE_RECEIVED, numBytes, -1, mmBuffer);
                writtenMsg.sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);

                // Send a failure message back to the activity.
                Message writeErrorMsg =
                        handler.obtainMessage(MessageConstants.STATE_CONNECTION_FAILED);
                Bundle bundle = new Bundle();
                bundle.putString("toast",
                        "Couldn't send data to the other device");
                writeErrorMsg.setData(bundle);
                handler.sendMessage(writeErrorMsg);
            }
        }


    }

}


















