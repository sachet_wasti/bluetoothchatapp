package com.example.connecting_devices;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class DeviceListAdapter extends ArrayAdapter<DeviceListAdapter.ViewHolder> {

    private LayoutInflater m_layoutInflator;
    private ArrayList<BluetoothDevice> mDevices;
    private int mViewResourceId;

    static class ViewHolder{
        TextView device_name;
        TextView device_address;
        ViewHolder(View view){
            device_name = view.findViewById(R.id.device_name);
            device_address = view.findViewById(R.id.address);
        }
    }

    public DeviceListAdapter(Context context, int resource, ArrayList<BluetoothDevice> mDevices) {
        super(context, resource);
        this.m_layoutInflator = LayoutInflater.from(context);
        this.mDevices = mDevices;
        this.mViewResourceId = resource;
    }

    @Override
    public int getCount() {
        return mDevices.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View newView = convertView;
        ViewHolder holder = null;

        if (newView == null){
            newView = m_layoutInflator.inflate(mViewResourceId, null);
            holder = new ViewHolder(newView);
            newView.setTag(holder);
        }else {
            holder = (ViewHolder)newView.getTag();
        }

        BluetoothDevice device = mDevices.get(position);

        if (device != null){
            holder.device_name.setText(device.getName());
            holder.device_address.setText(device.getAddress());
        }

        return newView;
    }

}
